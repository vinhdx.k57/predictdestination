export default {
    tabs: {
        home: 'Trang chủ',
        predict: 'Xem',
        setting: 'Liên hệ'
    },
    retry: 'Thử lại',
    userName: 'Họ và tên của bạn',
    phoneNumber: 'Số điện thoại',
    cmt: 'Số chứng ming thư',
    send: 'Gửi',
    email: 'Email',
    address: 'Địa chỉ',
    yourQuestion: 'Câu hỏi của bạn',

}