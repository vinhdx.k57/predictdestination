/**
 * App Navigation
 */
import React from 'react';
import { Actions, Scene, Stack, Router, Drawer, Lightbox, Overlay } from 'react-native-router-flux';
import SplashScreen from '../screen/SplashScreen';
import Main from './Main';

/* Routes ==================================================================== */
export default Actions.create(
  <Overlay >
    <Lightbox>
      <Stack key="root" hideNavBar panHandlers={null}>
        <Scene key="splash" initial hideNavBar component={SplashScreen} />
        
        {Main}

      </Stack>

    </Lightbox>
  </Overlay>
);
