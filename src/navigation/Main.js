/**
 * Tabs Scenes
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React from 'react';
import { Scene, Tabs, Stack, ActionConst } from 'react-native-router-flux';

// Consts and Libs
import  AppColors from '../theme/colors';

import HomeScene from './tabs/HomeScene';
import SettingScene from './tabs/SettingScene';
import PredictionScene from './tabs/PredictionScene';


/* Routes ==================================================================== */
export default (
    <Tabs
        key='main'
        swipeEnabled={false}
        showLabel={false}
        tabBarPosition='bottom'
        wrap={false}
        // activeBackgroundColor={AppColors.tabbar.background.inactive}
        // inactiveBackgroundColor={AppColors.tabbar.background.active}
        lazy={true}
    >
        {HomeScene}
        {PredictionScene}
        {SettingScene}
    </Tabs>
);
