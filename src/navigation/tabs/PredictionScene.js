import React from 'react';
import { Scene, Tabs, Stack, ActionConst, Text } from 'react-native-router-flux';


// Consts and Libs
import Messages from '../../constants/Messages';
import { AppStyles, AppColors } from '../../theme/index';

// Scenes

import TabIcon from '../../components/TabIcon';
import PredictScreen from '../../screen/PredictionScreen';


export default (
  <Stack key='predictTab'
    // tabBarLabel={Messages.tabs.home}
    hideNavBar
    inactiveBackgroundColor={AppColors.tabbar.background.inactive}
    activeBackgroundColor={AppColors.tabbar.background.active}
    navigationBarStyle={AppStyles.navbar}
    titleStyle={AppStyles.navbarTitle}
    title = {Messages.tabs.predict}
    icon={TabIcon}
    iconActive={'search'}
    iconInactive={'search'}
  >
    <Scene key='predict' component={PredictScreen} />
    
</Stack>
);