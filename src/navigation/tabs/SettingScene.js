import React from 'react';
import { Scene, Tabs, Stack, ActionConst, Text } from 'react-native-router-flux';


// Consts and Libs
import Messages from '../../constants/Messages';
import { AppStyles, AppColors } from '../../theme/index';

// Scenes

import TabIcon from '../../components/TabIcon';
import PredictScreen from '../../screen/PredictionScreen';
import SettingScreen from '../../screen/SettingScreen';


export default (
  <Stack key='settingTab'
    // tabBarLabel={Messages.tabs.home}
    hideNavBar
    inactiveBackgroundColor={AppColors.tabbar.background.inactive}
    activeBackgroundColor={AppColors.tabbar.background.active}
    navigationBarStyle={AppStyles.navbar}
    titleStyle={AppStyles.navbarTitle}
    title = {Messages.tabs.setting}
    icon={TabIcon}
    iconActive={'perm-contact-calendar'}
    iconInactive={'perm-contact-calendar'}
  >
    <Scene key='setting' component={SettingScreen} />
    
</Stack>
);