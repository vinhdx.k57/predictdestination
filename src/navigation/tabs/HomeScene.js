import React from 'react';
import { Scene, Tabs, Stack, ActionConst, Text } from 'react-native-router-flux';


// Consts and Libs
import Messages from '../../constants/Messages';
import { AppStyles, AppColors } from '../../theme/index';

// Scenes

import TabIcon from '../../components/TabIcon';
import HomeScreen from '../../screen/HomeScreen';


export default (
  <Stack key='homeTab'
    // tabBarLabel={Messages.tabs.home}
    hideNavBar
    inactiveBackgroundColor={AppColors.tabbar.background.inactive}
    activeBackgroundColor={AppColors.tabbar.background.active}
    navigationBarStyle={AppStyles.navbar}
    titleStyle={AppStyles.navbarTitle}
    title = {Messages.tabs.home}
    icon={TabIcon}
    iconActive={'home'}
    iconInactive={'home'}
  >
    <Scene key='home' component={HomeScreen} />
    
</Stack>
);