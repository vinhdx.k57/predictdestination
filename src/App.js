/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  ImageBackground,
  FlatList,
  TouchableOpacity,
  ScrollView,
  Button
} from 'react-native';
import { Router, Scene, Actions } from 'react-native-router-flux';
import AppRoutes from './navigation/index';


class App extends Component {
  constructor(props) {
    super(props);
  }


  render() {

    return (
      <View style={{ width: '100%', height: '100%' }}>
          <Router scenes={AppRoutes} style={{ width: '100%', height: '100%', backgroundColor: '#fff', }} />
      </View>
    );
  }
}

export default App
