import React, { Component } from 'react';
import { View, StyleSheet, Text, Button, FlatList, TouchableOpacity, Dimensions, TextInput } from 'react-native';

import { Actions } from 'react-native-router-flux';
import { AppColors, AppStyles, AppSizes } from '@theme';
import Messages from '../constants/Messages';
import { CheckBox } from 'react-native-elements'


class PredictScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            mode: Messages.phoneNumber
        }
    }

    onClickSend() {

    }

    onClickCheckBox(item) {
        this.setState({ mode: item })
    }

    renderCheckbox(content, isChecked) {
        return <CheckBox
            left
            title={content}
            checked={isChecked}
            onPress={() => { this.onClickCheckBox(content) }}
            textStyle={styles.sectionText}
            containerStyle={{ width: AppSizes.screen.width - 32, backgroundColor: 'white', marginLeft: 0, marginTop: 16 }}
        />
    }
    render() {
        return <View style={styles.container}>

            <View style={{ height: '100%', width: '100%', alignItems: 'center', paddingTop: 30 }}>
                <TextInput
                    style={[AppStyles.textInput, styles.textInput]}
                    autoCapitalize='none'
                    autoCorrect={false}
                    underlineColorAndroid='transparent'
                    returnKeyType='next'
                    placeholder={Messages.userName}
                    placeholderTextColor={AppColors.textSecondary}
                    onChangeText={userName => this.setState({ userName })}
                // value={this.state.userName}
                />

                {this.renderCheckbox(Messages.phoneNumber, this.state.mode === Messages.phoneNumber)}
                {this.renderCheckbox(Messages.cmt, this.state.mode === Messages.cmt)}
                {this.state.mode === Messages.phoneNumber ? <TextInput
                    style={[AppStyles.textInput, styles.textInput]}
                    autoCapitalize='none'
                    autoCorrect={false}
                    underlineColorAndroid='transparent'
                    returnKeyType='next'
                    keyboardType={'phone-pad'}
                    placeholder={Messages.phoneNumber}
                    placeholderTextColor={AppColors.textSecondary}
                    onChangeText={numberPhone => this.setState({ phoneNumber })}
                // value={this.state.numberPhone}
                />
                    :
                    <TextInput
                        ref='userNameInput'
                        style={[AppStyles.textInput, styles.textInput]}
                        autoCapitalize='none'
                        autoCorrect={false}
                        underlineColorAndroid='transparent'
                        returnKeyType='next'
                        placeholder={Messages.cmt}
                        placeholderTextColor={AppColors.textSecondary}
                        onChangeText={email => this.setState({ cmt })}
                    // value={this.state.email}
                    />}

                <TouchableOpacity style={[AppStyles.textInput, styles.textInput, styles.containerButtonSend]} onPress={() => this.onClickSend()}>
                    <Text style={{ color: 'white', fontSize: 16 }}>{Messages.send}</Text>
                </TouchableOpacity>
            </View>
        </View>
    }
};


export default PredictScreen

const styles = StyleSheet.create({

    container: {
        width: '100%',
        height: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    textInput: {
        marginLeft: AppSizes.paddingMedium,
        marginRight: AppSizes.paddingMedium,
        marginTop: AppSizes.paddingSml,
        marginBottom: AppSizes.paddingSml,
        width: AppSizes.screen.width - AppSizes.paddingMedium * 2
    },
    containerButtonSend: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: AppColors.themeApp
    },
    sectionText: {
        fontSize: 12,
        fontWeight: 'normal',
        color: 'grey'
    }


});