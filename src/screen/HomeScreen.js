import React, { Component } from 'react';
import { View, StyleSheet, Text, Button, FlatList, TouchableOpacity, Dimensions, ActivityIndicator } from 'react-native';

import { Actions } from 'react-native-router-flux';
import { AppColors, AppStyles, AppSizes } from '@theme';
import { WebView } from 'react-native-webview';
import Messages from '../constants/Messages';


class HomeScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: true, 
            
        }
    }
    renderError() {
        return <View style={[styles.container, {}]}>
            <View style={{ width: AppSizes.screen.width, justifyContent: 'center' }}>
                <Text style={styles.textError}>{"Lỗi kết nối mạng"}</Text>
                <TouchableOpacity activeOpacity={0.8} onPress={() => this.webView.reload()} style={styles.buttonRetry}>
                    <Text style={styles.textButtonRetry}>{Messages.retry}</Text>
                </TouchableOpacity>
            </View>

        </View >
    }
   
    render() {
        return <View style={styles.container}>

           {this.state.loading && <View style={[styles.container, { height: AppSizes.screen.height, justifyContent: 'center' }]}>
                <ActivityIndicator />
            </View>}
            <WebView
                ref={ref => this.webView = ref}

                source={{ uri: 'https://github.com/xotahal/react-native-motion?fbclid=IwAR2RgzhXxTP1_uAIQi7lvbqL1nHR-HUuzTeLCEdqdS_RYR729fnMTz0jlXg' }}
                style={{ width: AppSizes.screen.width, height: AppSizes.screen.height - 50 }}

                onLoadEnd={() => this.setState({ loading: false })}
                renderError={() => this.renderError()}
            />

        </View>
    }
};


export default HomeScreen

const styles = StyleSheet.create({

    container: {
        width: '100%',
        height: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    textError: {
        width: '100%',
        textAlign: 'center',
        fontSize: 16,
        color: 'black',
    },
    buttonRetry: {
        padding: 16,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent'

    },
    textButtonRetry: {
        fontSize: 16,
        color: AppColors.textContent,
        backgroundColor: 'transparent'
    },

});