import React, { Component } from 'react';
import { View, StyleSheet, Text, Button, FlatList, TouchableOpacity, Dimensions, TextInput } from 'react-native';

import { Actions } from 'react-native-router-flux';
import { AppColors, AppStyles, AppSizes } from '@theme';
import Messages from '../constants/Messages';


class SettingScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    onClickSend() {

    }
    render() {
        return <View style={styles.container}>

            <View style={{ height: '100%', width: '100%', alignItems: 'center', paddingTop: 30 }}>
                <TextInput
                    style={[AppStyles.textInput, styles.textInput]}
                    autoCapitalize='none'
                    autoCorrect={false}
                    underlineColorAndroid='transparent'
                    returnKeyType='next'
                    placeholder={Messages.userName}
                    placeholderTextColor={AppColors.textSecondary}
                    onChangeText={userName => this.setState({ userName })}
                // value={this.state.userName}
                />
                <TextInput
                    style={[AppStyles.textInput, styles.textInput]}
                    autoCapitalize='none'
                    autoCorrect={false}
                    underlineColorAndroid='transparent'
                    returnKeyType='next'
                    placeholder={Messages.email}
                    placeholderTextColor={AppColors.textSecondary}
                    onChangeText={userName => this.setState({ email })}
                // value={this.state.userName}
                />
                <TextInput
                    style={[AppStyles.textInput, styles.textInput]}
                    autoCapitalize='none'
                    autoCorrect={false}
                    underlineColorAndroid='transparent'
                    returnKeyType='next'
                    placeholder={Messages.phoneNumber}
                    placeholderTextColor={AppColors.textSecondary}
                    onChangeText={userName => this.setState({ phoneNumber })}
                // value={this.state.userName}
                />
                <TextInput
                    style={[AppStyles.textInput, styles.textInput]}
                    autoCapitalize='none'
                    autoCorrect={false}
                    underlineColorAndroid='transparent'
                    returnKeyType='next'
                    placeholder={Messages.address}
                    placeholderTextColor={AppColors.textSecondary}
                    onChangeText={userName => this.setState({ address })}
                // value={this.state.userName}
                />
                <TextInput
                    style={[AppStyles.textInput, styles.textInput]}
                    autoCapitalize='none'
                    autoCorrect={false}
                    underlineColorAndroid='transparent'
                    returnKeyType='next'
                    placeholder={Messages.yourQuestion}
                    placeholderTextColor={AppColors.textSecondary}
                    onChangeText={userName => this.setState({ yourQuestion })}
                // value={this.state.userName}
                />

                <TouchableOpacity style={[AppStyles.textInput, styles.textInput, styles.containerButtonSend]} onPress={() => this.onClickSend()}>
                    <Text style={{ color: 'white', fontSize: 16 }}>{Messages.send}</Text>
                </TouchableOpacity>
            </View>
        </View>
    }
};


export default SettingScreen

const styles = StyleSheet.create({

    container: {
        width: '100%',
        height: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    textInput: {
        marginLeft: AppSizes.paddingMedium,
        marginRight: AppSizes.paddingMedium,
        marginTop: AppSizes.paddingSml,
        marginBottom: AppSizes.paddingSml,
        width: AppSizes.screen.width - AppSizes.paddingMedium * 2
    },
    containerButtonSend: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: AppColors.themeApp
    },
    sectionText: {
        fontSize: 12,
        fontWeight: 'normal',
        color: 'grey'
    }

});