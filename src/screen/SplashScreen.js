import React, { Component } from 'react';
import { View, StyleSheet, Text, Button, Image } from 'react-native';
import { Actions, ActionConst } from 'react-native-router-flux';

class SplashScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    componentDidMount() {
        setTimeout(function () { Actions.main() }, 2000);

    }
    //UI CONTROL ---------------------------------------------------------------------------------


    //UI RENDER ----------------------------------------------------------------------------------
    render() {
        return <View style={styles.container}>
            <Text>Splash Screen</Text>
        </View>
    }
};

export default SplashScreen

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white'
    },
    text: {
        padding: 5,
    }
});