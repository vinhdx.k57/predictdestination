
const UnauthorizeStatusCode = 401;

export default UnauthorizeInterceptor = {
  onFullfilled: (response) => {
    return Promise.resolve(response);
  },

  onRejected: (error) => {
    if (error) {

      
      return Promise.reject(error);
    }
  }
}