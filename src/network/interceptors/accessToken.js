export default AccessTokenInterceptor = {
  addAccessToken: (config) => {
    if (accessToken) {
      const headers = { ...config.headers, 'Content-Type': 'application/json' };
      config.headers = headers;
    }
    return config;
  },

  onRejected: (error) => {
    return Promise.reject(error);
  }
}